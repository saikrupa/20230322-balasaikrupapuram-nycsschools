package com.test.a20230323_balasaikrupapuram_nycschools.di

import com.test.a20230323_balasaikrupapuram_nycschools.data.api.NYCHighSchoolsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NYCHighSchools {

    @Singleton
    @Provides
    fun provideHighSchoolsService(retrofit: Retrofit): NYCHighSchoolsApi =
        retrofit.create(NYCHighSchoolsApi::class.java)

}