package com.test.a20230323_balasaikrupapuram_nycschools.core

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

// We can handle the failure related common logic here as it's global call
interface SafeApiCall {
    suspend fun <T> safeApiCall(
        apiCall: suspend () -> T
    ): Resource<T> {
        return withContext(Dispatchers.IO) {
            try {
                Resource.Loading
                Resource.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is HttpException -> {
                        Resource.Failure(
                            false,
                            throwable.code(),
                            throwable.response()?.errorBody(),
                            throwable
                        )
                    }
                    else -> {
                        Resource.Failure(true, null, null, throwable)
                    }
                }
            }
        }
    }
}
