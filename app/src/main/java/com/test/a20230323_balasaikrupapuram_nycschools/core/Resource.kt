package com.test.a20230323_balasaikrupapuram_nycschools.core

import okhttp3.ResponseBody
// This is useful for handling all the api calls specific errors in once place
sealed class Resource<out T> {
    data class Success<out T>(val value: T) : Resource<T>()
    data class Failure(
        val isNetworkError: Boolean,
        val errorCode: Int?,
        val errorBody: ResponseBody?,
        val throwable: Throwable
    ) : Resource<Nothing>()
    object Loading : Resource<Nothing>()
}
