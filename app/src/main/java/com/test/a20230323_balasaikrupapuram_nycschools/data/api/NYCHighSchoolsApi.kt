package com.test.a20230323_balasaikrupapuram_nycschools.data.api

import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.NYCHighSchoolsDataItem
import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.SATScoreDataItem
import retrofit2.http.GET

// Defining the end points here
interface NYCHighSchoolsApi {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getHighSchoolList(): List<NYCHighSchoolsDataItem>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATScores(): List<SATScoreDataItem>

}