package com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SATScoreDataItem(
    val dbn: String? = null,
    val num_of_sat_test_takers: String? = null,
    val sat_critical_reading_avg_score: String? = null,
    val sat_math_avg_score: String? = null,
    val sat_writing_avg_score: String? = null,
    val school_name: String? = null
) : Parcelable