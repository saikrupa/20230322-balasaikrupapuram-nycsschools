package com.test.a20230323_balasaikrupapuram_nycschools.data.repository

import com.test.a20230323_balasaikrupapuram_nycschools.core.Resource
import com.test.a20230323_balasaikrupapuram_nycschools.core.SafeApiCall
import com.test.a20230323_balasaikrupapuram_nycschools.data.api.NYCHighSchoolsApi
import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.NYCHighSchoolsDataItem
import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.SATScoreDataItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

// This is the class we used to fetch the data from API or from the local database
class HighSchoolsRepositoryImpl @Inject constructor(
    private val schoolsDirectoryAPi: NYCHighSchoolsApi
) : SafeApiCall {

    fun getHighSchoolsDirectory() : Flow<Resource<List<NYCHighSchoolsDataItem>>> = flow {
        try {
            val apiResponse = safeApiCall {
                schoolsDirectoryAPi.getHighSchoolList()
            }
            if (apiResponse is Resource.Success) {
                 val mainData = apiResponse.value
                 emit(Resource.Success(mainData))
            } else {
                emit(apiResponse)
            }
        } catch (e: Exception) {
            e.stackTrace
        }
    }

    fun getSATScoreData() : Flow<Resource<List<SATScoreDataItem>>> = flow {
        try {
            val apiResponse = safeApiCall {
                schoolsDirectoryAPi.getSATScores()
            }
            if (apiResponse is Resource.Success) {
                val mainData = apiResponse.value
                emit(Resource.Success(mainData))
            } else {
                emit(apiResponse)
            }
        } catch (e: Exception) {
            e.stackTrace
        }
    }
}
