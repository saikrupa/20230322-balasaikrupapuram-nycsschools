package com.test.a20230323_balasaikrupapuram_nycschools.presentation.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.a20230323_balasaikrupapuram_nycschools.core.Resource
import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.NYCHighSchoolsDataItem
import com.test.a20230323_balasaikrupapuram_nycschools.data.repository.HighSchoolsRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NYCHighSchoolListViewModel @Inject constructor(
    private val highSchoolsImpl: HighSchoolsRepositoryImpl,
) : ViewModel() {

    private val _sasUrlState = MutableStateFlow(HighSchoolsListUiState.Success(listOf()))
    val sasUrlState: StateFlow<HighSchoolsListUiState> = _sasUrlState

    init {
        viewModelScope.launch{
            getPendingApprovals()
        }
    }

    private suspend fun getPendingApprovals() {
        highSchoolsImpl.getHighSchoolsDirectory().collect {
            when (it) {
                is Resource.Success -> {
                    val state = _sasUrlState.value.copy(it.value)
                    _sasUrlState.emit(state)
                }
                is Resource.Loading -> {}
                is Resource.Failure -> {
                    Log.e("Error", it.errorBody.toString())
                }
            }
        }
    }

}
sealed class HighSchoolsListUiState {
    data class Success(val data: List<NYCHighSchoolsDataItem>) : HighSchoolsListUiState()
    data class Error(val exception: Throwable) : HighSchoolsListUiState()
}