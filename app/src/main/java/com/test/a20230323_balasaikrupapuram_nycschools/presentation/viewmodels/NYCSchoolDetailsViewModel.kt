package com.test.a20230323_balasaikrupapuram_nycschools.presentation.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.a20230323_balasaikrupapuram_nycschools.core.Resource
import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.SATScoreDataItem
import com.test.a20230323_balasaikrupapuram_nycschools.data.repository.HighSchoolsRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NYCSchoolDetailsViewModel @Inject constructor(
    private val highSchoolsImpl: HighSchoolsRepositoryImpl,
) : ViewModel() {

    private val _state = MutableStateFlow(HighSchoolsDetailedUiState.Success(listOf()))
    val urlState: StateFlow<HighSchoolsDetailedUiState> = _state

    init {
        viewModelScope.launch {
            getPendingApprovals()
        }
    }

    private suspend fun getPendingApprovals() {
        highSchoolsImpl.getSATScoreData().collect {
            when (it) {
                is Resource.Success -> {
                    val state = _state.value.copy(it.value)
                    _state.emit(state)
                }
                is Resource.Loading -> {}
                is Resource.Failure -> {
                    Log.e("Error", it.errorBody.toString())
                }
            }
        }
    }

}

sealed class HighSchoolsDetailedUiState {
    data class Success(val data: List<SATScoreDataItem>) : HighSchoolsDetailedUiState()
    data class Error(val exception: Throwable) : HighSchoolsDetailedUiState()
}