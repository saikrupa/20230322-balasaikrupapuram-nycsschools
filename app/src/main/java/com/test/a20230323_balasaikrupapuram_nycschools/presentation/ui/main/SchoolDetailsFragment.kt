package com.test.a20230323_balasaikrupapuram_nycschools.presentation.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.test.a20230323_balasaikrupapuram_nycschools.R
import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.NYCHighSchoolsDataItem
import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.SATScoreDataItem
import com.test.a20230323_balasaikrupapuram_nycschools.databinding.FragmentSchoolDetailsBinding
import com.test.a20230323_balasaikrupapuram_nycschools.presentation.viewmodels.HighSchoolsDetailedUiState
import com.test.a20230323_balasaikrupapuram_nycschools.presentation.viewmodels.NYCSchoolDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SchoolDetailsFragment : Fragment() {

    private lateinit var binding: FragmentSchoolDetailsBinding

    private var schoolDetails: NYCHighSchoolsDataItem? = null

    private val viewModel: NYCSchoolDetailsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolDetailsBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() {
        val args: SchoolDetailsFragmentArgs by navArgs()
        schoolDetails = args.schoolDetails
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.urlState.collect { it ->
                    when (it) {
                        is HighSchoolsDetailedUiState.Success -> {
                            val listSATScores = it.data
                            if(listSATScores.isNotEmpty()) {
                                val selectedSchool =
                                    listSATScores.firstOrNull { it.dbn == schoolDetails?.dbn }
                                assignDataToUi(selectedSchool, schoolDetails)
                            }
                        }
                        is HighSchoolsDetailedUiState.Error ->{
                            Log.e("Error", it.exception.toString())
                        }
                        else -> {}
                    }
                }
            }
        }
    }

    private fun assignDataToUi(selectedSchool: SATScoreDataItem?, schoolDetails: NYCHighSchoolsDataItem?) {
        binding.apply {
            tvSchoolName.text= schoolDetails?.school_name
            if(selectedSchool!=null) {
                tvMaths.text = selectedSchool.sat_math_avg_score
                tvReading.text = selectedSchool.sat_critical_reading_avg_score
                tvWriting.text = selectedSchool.sat_writing_avg_score
            }else{
                tvMaths.text = context?.resources?.getString(R.string.na)
                tvReading.text = context?.resources?.getString(R.string.na)
                tvWriting.text = context?.resources?.getString(R.string.na)
            }
        }
    }
}