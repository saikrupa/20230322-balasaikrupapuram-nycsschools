package com.test.a20230323_balasaikrupapuram_nycschools.presentation.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.a20230323_balasaikrupapuram_nycschools.databinding.FragmentNycHighSchoolsListBinding
import com.test.a20230323_balasaikrupapuram_nycschools.presentation.adapters.NYCHighSchoolsListAdapter
import com.test.a20230323_balasaikrupapuram_nycschools.presentation.viewmodels.HighSchoolsListUiState
import com.test.a20230323_balasaikrupapuram_nycschools.presentation.viewmodels.NYCHighSchoolListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NYCHighSchoolsListFragment : Fragment() {

    private lateinit var binding: FragmentNycHighSchoolsListBinding

    private var adapter = NYCHighSchoolsListAdapter()

    private val viewModel: NYCHighSchoolListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNycHighSchoolsListBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservers()
    }

    private fun initObservers() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.sasUrlState.collect {
                    when (it) {
                        is HighSchoolsListUiState.Success -> {
                            adapter.submitList(it.data)
                        }
                        is HighSchoolsListUiState.Error -> {
                            Log.e("Error", it.exception.toString())
                        }
                        else -> {}
                    }
                }
            }
        }
    }

    private fun initViews() {

        binding.apply {
            message.visibility = View.GONE
            rvNycList.visibility = View.VISIBLE
            rvNycList.addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )
            rvNycList.adapter = adapter
        }
    }
}

