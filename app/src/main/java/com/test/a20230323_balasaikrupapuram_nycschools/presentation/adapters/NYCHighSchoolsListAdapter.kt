package com.test.a20230323_balasaikrupapuram_nycschools.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.test.a20230323_balasaikrupapuram_nycschools.R
import com.test.a20230323_balasaikrupapuram_nycschools.data.remote.response.NYCHighSchoolsDataItem
import com.test.a20230323_balasaikrupapuram_nycschools.databinding.NycHighSchoolItemBinding
import com.test.a20230323_balasaikrupapuram_nycschools.presentation.ui.main.NYCHighSchoolsListFragmentDirections

// Adapter class to display the data on the recycler view
class NYCHighSchoolsListAdapter :
    ListAdapter<NYCHighSchoolsDataItem, NYCHighSchoolsListAdapter.MainViewHolder>(DIFF_CALLBACK) {

    // used to identify the similar data objects so that we will not assign the same data to the list
    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<NYCHighSchoolsDataItem>() {
            override fun areItemsTheSame(
                oldItem: NYCHighSchoolsDataItem,
                newItem: NYCHighSchoolsDataItem
            ): Boolean = oldItem.dbn == newItem.dbn

            override fun areContentsTheSame(
                oldItem: NYCHighSchoolsDataItem,
                newItem: NYCHighSchoolsDataItem
            ): Boolean = oldItem == newItem
        }
    }

    inner class MainViewHolder(val binding: NycHighSchoolItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        return MainViewHolder(
            NycHighSchoolItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val item = getItem(position)
        // Setting up the data to the screen
        holder.binding.apply {
            tvSchoolImage.text = item?.school_name?.get(0).toString()
            tvSchoolName.text= item?.school_name
            tvSchoolAddress.text = String.format(
                holder.itemView.context.resources.getString(R.string.address),
                item.city,
                item.state_code,
                item.zip
            )
            tvPhone.text = item.phone_number
            tvEmail.text = item.school_email
        }
        holder.binding.apply {
            root.setOnClickListener {
                val action = NYCHighSchoolsListFragmentDirections.actionNYCHighSchoolsListFragmentToSchoolDetailsFragment(item)
                holder.itemView.findNavController().navigate(action)
            }
        }
    }
}